<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    {foreach item=style from=$css}
        <link href="{$style}" rel="stylesheet">
    {/foreach}
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=no" />
    <title>Invisio</title>
</head>