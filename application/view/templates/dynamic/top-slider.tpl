<div class="scrollto" style="z-index:0;">
    <div class="page full-width">

        <!--top-slider-start-->

        <div class="full-width full-h">
            <div class="fix-baner">
                <div class="swiper-container top-baner">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide active" data-val="0">
                            <div class="bg">
                                <img class="center_img onload" src="public/img/bg.jpg" data-width-img="1920" data-height-img="1080" onload="this.setAttribute('class', 'center_img onload loaded');"/>
                            </div>
                            <div class="slider-top-thumbs">
                                <div class="row-fluid table">
                                    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-12 col-xs-12 t_td">
                                        <h1 class="color-5acdc4 animation-scale">invisio <span class="animation-opacity animation-800">say`s</span> Hello!</h1>
                                        <div class="row-fluid">
                                            <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-12">
                                                <div class="row-fluid">
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <img class="animation-opacity" src="public/img/icon.png"/>
                                                        <br><span class="animation-rotate animation-500">progressive</span>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <img class="animation-opacity animation-500" src="public/img/icon1.png"/>
                                                        <br><span class="animation-rotate animation-800">creative</span>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <img class="animation-opacity animation-800" src="public/img/icon2.png"/>
                                                        <br><span class="animation-rotate">Love games</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide" data-val="1">
                            <div class="bg">
                                <img class="center_img onload" src="public/img/bg.jpg" data-width-img="1920" data-height-img="1080" onload="this.setAttribute('class', 'center_img onload loaded');"/>
                            </div>
                            <div class="slider-top-thumbs">
                                <div class="row-fluid table">
                                    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-12 col-xs-12 t_td">
                                        <h1 class="color-5acdc4 animation-scale"><span class="animation-opacity animation-500">Always </span>in trend</h1>
                                        <div class="row-fluid">
                                            <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-12">
                                                <div class="row-fluid">
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <img class="animation-opacity" src="public/img/responsive_icon.png"/>
                                                        <br><span class="animation-rotate animation-500">responsive</span>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <img class="animation-opacity animation-500" src="public/img/app_icon.png"/>
                                                        <br><span class="animation-rotate animation-800">app</span>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <img class="animation-opacity animation-800" src="public/img/rmobile_icon.png"/>
                                                        <br><span class="animation-rotate">mobile</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide" data-val="2">
                            <div class="bg">
                                <img class="center_img onload" src="public/img/bg.jpg" data-width-img="1920" data-height-img="1080" onload="this.setAttribute('class', 'center_img onload loaded');"/>
                            </div>
                            <div class="slider-top-thumbs">
                                <div class="row-fluid table">
                                    <div class="ccol-lg-6 col-lg-offset-3 ol-md-6 col-md-offset-3 col-sm-12 col-xs-12 t_td">
                                        <h1 class="color-5acdc4 animation-scale">Look in <span class="animation-opacity animation-500">future</span></h1>
                                        <div class="row-fluid">
                                            <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-12">
                                                <div class="row-fluid">
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <img class="animation-opacity" src="public/img/multiplatform_icon.png"/>
                                                        <br><span class="animation-rotate animation-500">multiplatform</span>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <img class="animation-opacity animation-500" src="public/img/3d_hd_icon.png"/>
                                                        <br><span class="animation-rotate animation-800">3d</span>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <img class="animation-opacity animation-800" src="public/img/ultra_hd_icon.png"/>
                                                        <br><span class="animation-rotate">ultra hd</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide" data-val="3">
                            <div class="bg">
                                <img class="center_img onload" src="public/img/bg.jpg" data-width-img="1920" data-height-img="1080" onload="this.setAttribute('class', 'center_img onload loaded');"/>
                            </div>
                            <div class="slider-top-thumbs">
                                <div class="row-fluid table">
                                    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-12 col-xs-12 t_td">
                                        <h1 class="color-5acdc4 animation-scale">Love our <span class="animation-opacity animation-500">customers</span></h1>
                                        <div class="row-fluid">
                                            <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-12">
                                                <div class="row-fluid">
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <img class="animation-opacity" src="public/img/listen_icon.png"/>
                                                        <br><span class="animation-rotate animation-500">listen</span>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <img class="animation-opacity animation-500" src="public/img/explain_icon.png"/>
                                                        <br><span class="animation-rotate animation-800">explain</span>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <img class="animation-opacity animation-800" src="public/img/create_icon.png"/>
                                                        <br><span class="animation-rotate">create</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pagination swich_1"></div>
                    <div class="slider-prev">&#212;</div>
                    <div class="slider-next">&#215;</div>
                    <div class="wheel-wrap">
                        <div class="wheel">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--top-slider-end-->