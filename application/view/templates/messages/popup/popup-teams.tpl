<!--popup-team-->

<div class="popup-team-container">
    <div class="popup-container-team popup-slider-team">
        <div class="swiper-wrapper align-top">
            <div class="swiper-slide popup-team">
                <div class="wrap-team">
                    <div class="left-content">
                        <img src="public/img/team.jpg" alt="" class="onload"  onload="this.setAttribute('class', 'onload loaded');">
                    </div>
                    <div class="right-wrap">
                        <div class="right-content">
                            <button class="team-button2 color_fff">
                                <b>John Dou</b></br>
                                <i>company founder</i>
                            </button>
                            <div class="clear"></div>
                            <p>Aliquam id tincidunt turpis, eu fringilla erat. Vestibulum lacinia, nisi sed bibendum pretium, eros sapien laoreet metus, ac facilisis felis enim a nibh. Sed sed ligula tellus. Aliquam feugiat, nunc ut tincidunt laoreet, urna quam pellentesque diam, id posuere velit lectus vel dui. Maecenas posuere eget metus ac venenatis. Pellentesque eget quam luctus, molestie ligula eu, blandit orci. Vestibulum vel nulla non quam ornare lobortis ut id enim. Praesent tempor porttitor dui vel adipiscing. Vestibulum in tristique lacus, eu dignissim augue.
                            </p>
                            <div class="team-icon">
                                <div class="roll-batton">
                                    <img src="public/img/team-fb.png" alt="">
                                    <img src="public/img/team-fb-hov.png" class="roll" alt="">
                                </div>
                                <div class="roll-batton">
                                    <img src="public/img/team-tw.png" alt="">
                                    <img src="public/img/team-tw-hov.png" class="roll" alt="">
                                </div>
                                <div class="roll-batton">
                                    <img src="public/img/team-in.png" alt="">
                                    <img src="public/img/team-in-hov.png" class="roll" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="close-up class">
                        <span class="close-x">+</span>
                    </div>
                </div>
            </div>
            <div class="swiper-slide popup-team">
                <div class="wrap-team">
                    <div class="left-content">
                        <img src="public/img/team.jpg" alt="" class="onload"  onload="this.setAttribute('class', 'onload loaded');">
                    </div>
                    <div class="right-content">
                        <button class="team-button2 color_fff">
                            <b>Kerry Smith</b></br>
                            <i>co-founder</i>
                        </button>
                        <div class="clear"></div>
                        <p>Aliquam id tincidunt turpis, eu fringilla erat. Vestibulum lacinia, nisi sed bibendum pretium, eros sapien laoreet metus, ac facilisis felis enim a nibh. Sed sed ligula tellus. Aliquam feugiat, nunc ut tincidunt laoreet, urna quam pellentesque diam, id posuere velit lectus vel dui. Maecenas posuere eget metus ac venenatis. Pellentesque eget quam luctus, molestie ligula eu, blandit orci. Vestibulum vel nulla non quam ornare lobortis ut id enim. Praesent tempor porttitor dui vel adipiscing. Vestibulum in tristique lacus, eu dignissim augue.
                        </p>
                        <div class="team-icon">
                            <div class="roll-batton">
                                <img src="public/img/team-fb.png" alt="">
                                <img src="public/img/team-fb-hov.png" class="roll" alt="">
                            </div>
                            <div class="roll-batton">
                                <img src="public/img/team-tw.png" alt="">
                                <img src="public/img/team-tw-hov.png" class="roll" alt="">
                            </div>
                            <div class="roll-batton">
                                <img src="public/img/team-in.png" alt="">
                                <img src="public/img/team-in-hov.png" class="roll" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="close-up class">
                        <span class="close-x">+</span>
                    </div>
                </div>
            </div>
            <div class="swiper-slide popup-team">
                <div class="wrap-team">
                    <div class="left-content">
                        <img src="public/img/team.jpg" alt="" class="onload" onload="this.setAttribute('class', 'onload loaded');"/>
                    </div>
                    <div class="right-content">
                        <button class="team-button2 color_fff">
                            <b>Peter Chester</b></br>
                            <i>co-founder</i>
                        </button>
                        <div class="clear"></div>
                        <p>Aliquam id tincidunt turpis, eu fringilla erat. Vestibulum lacinia, nisi sed bibendum pretium, eros sapien laoreet metus, ac facilisis felis enim a nibh. Sed sed ligula tellus. Aliquam feugiat, nunc ut tincidunt laoreet, urna quam pellentesque diam, id posuere velit lectus vel dui. Maecenas posuere eget metus ac venenatis. Pellentesque eget quam luctus, molestie ligula eu, blandit orci. Vestibulum vel nulla non quam ornare lobortis ut id enim. Praesent tempor porttitor dui vel adipiscing. Vestibulum in tristique lacus, eu dignissim augue.
                        </p>
                        <div class="team-icon">
                            <div class="roll-batton">
                                <img src="public/img/team-fb.png" alt="">
                                <img src="public/img/team-fb-hov.png" class="roll" alt="">
                            </div>
                            <div class="roll-batton">
                                <img src="public/img/team-tw.png" alt="">
                                <img src="public/img/team-tw-hov.png" class="roll" alt="">
                            </div>
                            <div class="roll-batton">
                                <img src="public/img/team-in.png" alt="">
                                <img src="public/img/team-in-hov.png" class="roll" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="close-up class">
                        <span class="close-x">+</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="slider-prev">&#212;</div>
        <div class="slider-next">&#215;</div>
    </div>
</div>