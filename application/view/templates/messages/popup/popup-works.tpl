<!--popup-works-->

<div class="popup-work-container">
    <div class="popup-container popup-slider">
        <div class="swiper-wrapper align-center">
            <div class="swiper-slide popup-slide">
                <div class="img-pp">
                    <img src="public/img/work1_big.jpg" alt="" class="onload" onload="this.setAttribute('class', 'onload loaded');">
                    <div class="gallery-title color_fff">
                        </br><span>promo</span>
                        </br><b>dj awards 2014</b>
                        </br><i>www.dj-promo2014.com</i>
                    </div>
                    <div class="close-up class">
                        <span class="close-x">+</span>
                    </div>
                </div>
            </div>
            <div class="swiper-slide popup-slide">
                <div class="img-pp">
                    <img src="public/img/work1_big.jpg" alt="" class="onload" onload="this.setAttribute('class', 'onload loaded');">
                    <div class="gallery-title color_fff">
                        </br><span>promo</span>
                        </br><b>dj awards 2014</b>
                        </br><i>www.dj-promo2014.com</i>
                    </div>
                    <div class="close-up class">
                        <span class="close-x">+</span>
                    </div>
                </div>
            </div>
            <div class="swiper-slide popup-slide">
                <div class="img-pp">
                    <img src="public/img/work1_big.jpg" alt="" class="onload" onload="this.setAttribute('class', 'onload loaded');">
                    <div class="gallery-title color_fff">
                        </br><span>promo</span>
                        </br><b>dj awards 2014</b>
                        </br><i>www.dj-promo2014.com</i>
                    </div>
                    <div class="close-up class">
                        <span class="close-x">+</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="slider-prev">&#212;</div>
        <div class="slider-next">&#215;</div>
    </div>
</div>