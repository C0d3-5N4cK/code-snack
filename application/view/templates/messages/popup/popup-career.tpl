<!--popup-career-->

<div class="career-popup-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="career-popup">
                    <div class="top-career-sity"><i>Washington, USA</i></div><br>
                    <div class="top-career-jobs"><b>senior web-developer</b></div><br>
                    <div class="top-career-program"><h5>Experience in PHP and HTML5/CSS3 is required</h5></div>
                    <div class="career-image"><img src="public/img/career-popup.jpg" alt="career"></div>
                    <div class="popup-text">
                        <p>Aliquam id tincidunt turpis, eu fringilla erat. Vestibulum lacinia, nisi sed bibendum pretium, eros sapien laoreet metus, ac facilisis felis enim a nibh. Sed sed ligula tellus. Aliquam feugiat, nunc ut tincidunt laoreet, urna quam pellentesque diam, id posuere velit lectus vel dui. Maecenas posuere eget metus ac venenatis. Pellentesque eget quam luctus, molestie ligula eu, blandit orci. Vestibulum vel nulla non quam ornare lobortis ut id enim. Praesent tempor porttitor dui vel adipiscing. Vestibulum in tristique lacus, eu dignissim augue. Pellentesque tristique, nulla sed varius pharetra, tortor massa varius eros, id porttitor mauris ligula quis nunc.Aliquam id tincidunt turpis, eu fringilla erat. Vestibulum lacinia, nisi sed bibendum pretium, eros sapien laoreet metus, ac facilisis felis enim a nibh. Sed sed ligula tellus. Aliquam feugiat, nunc ut tincidunt laoreet, urna quam pellentesque diam, id posuere velit lectus vel dui. Maecenas posuere eget metus ac venenatis. Pellentesque eget quam luctus, molestie ligula eu, blandit orci. Vestibulum vel nulla non quam ornare lobortis ut id enim. Praesent tempor porttitor dui vel adipiscing. Vestibulum in tristique lacus, eu dignissim augue. Pellentesque tristique, nulla sed varius pharetra, tortor massa varius eros, id porttitor mauris ligula quis nunc.Aliquam id tincidunt turpis, eu fringilla erat. Vestibulum lacinia, nisi sed bibendum pretium, eros sapien laoreet metus, ac facilisis felis enim a nibh. Sed sed ligula tellus. Aliquam feugiat, nunc ut tincidunt laoreet, urna quam pellentesque diam.</p>
                    </div>
                    <div class="close-up class">
                        <span class="close-x">+</span>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <input type="submit" value="contact us">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>