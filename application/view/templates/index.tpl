{include file="./static/header.tpl"}
<body class="dark">
<div class="loader">
    <img src="public/img/loader.GIF" alt="">
</div>
<div class="content-block">
    <div class="content-main">
        {include file="./dynamic/navigation.tpl"}
        {include file="./dynamic/top-slider.tpl"}
        <!--ABOUT US -->

        <div class="scrollto">
            <div class="full-width bg_5acdc4 about">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="content-title content scale-text">
                                <h2 class="color_fff time-line">ABOUT US</h2>
                                </br><img class="divid" src="public/img/separate-white.png" alt="">
                                <p class="color_fff">Aliquam id tincidunt turpis, eu fringilla erat. Vestibulum lacinia, nisi sed bibendum pretium, eros sapien laoreet metus, ac facilisis felis enim a nibh. Sed sed ligula tellus. Aliquam feugiat, nunc ut tincidunt laoreet, urna quam pellentesque diam, id posuere velit lectus vel dui. Maecenas posuere eget metus ac venenatis. Pellentesque eget quam luctus, molestie ligula eu, tincidunt turpis, eu fringilla erat. Curabitur et justo urna. Praesent vulputate dui ac massa venenatis viverra. Aenean sed egestas elit, sit amet ornare urna. Sed eget diam nunc.
                                </p>
                                <q class="color_fff">Sed porttitor ferment ipsum, ac cursus purus</q>
                            </div>
                            <img class="bottom-img" src="public/img/device.png" alt="">
                        </div>
                    </div>
                </div>
            </div>

            <!--COMPANY INFO-->

            <div class="full-width" style="background-image: url(public/img/company.jpg); background-position: center center; background-repeat: no-repeat; -webkit-background-size: cover;-moz-background-size: cover; -o-background-size: cover; background-size: cover; height:100%;">
                <div class="container">
                    <div class="row">
                        <div class="info scale-text">
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 info-animate">
                                <div class="info-block">
                                    <b class="color_fff">215</b></br>
                                    <h4 class="color_fff">Projects created</h4></br>
                                    <img class="divid" src="public/img/separate-white.png" alt="">
                                    </br><img class="info-icon" src="public/img/info1.png" alt="">
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 info-animate">
                                <div class="info-block">
                                    <b class="color_fff">1850</b></br>
                                    <h4 class="color_fff">Coffe drunk</h4></br>
                                    <img class="divid" src="public/img/separate-white.png" alt="">
                                    </br><img class="info-icon" src="public/img/info2.png" alt="">
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 info-animate">
                                <div class="info-block">
                                    <b class="color_fff">92</b></br>
                                    <h4 class="color_fff">Creative awards</h4></br>
                                    <img class="divid" src="public/img/separate-white.png" alt="">
                                    </br><img class="info-icon" src="public/img/info3.png" alt="">
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 info-animate">
                                <div class="info-block">
                                    <b class="color_fff">103</b></br>
                                    <h4 class="color_fff">Happy clients</h4></br>
                                    <img class="divid" src="public/img/separate-white.png" alt="">
                                    </br><img class="info-icon" src="public/img/info4.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!--SERVICES -->

        <div class="scrollto">
            <div class="full-width bg_f9f9f9">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="content content-title scale-text">
                                <h2 class="color_303030 timeIn">Maybe we can help you</h2>
                                </br><img src="public/img/separate-black.png" class="divid timeIn" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="row scale-text">
                        <div class="service">
                            <div class="col-md-4 col-sm-6 col-xs-12 service-animate">
                                <div class="service-block">
                                    <img src="public/img/serv-icon.png" alt="">
                                    <span class="title">Hi end design</span>
                                    <ul>
                                        <li>&#8195;HD Design</li>
                                        <li>&#8195;Responsive &amp; retina ready design</li>
                                        <li>&#8195;Extensive + powerful themes</li>
                                        <li>&#8195;Only creative and worked ideas</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12 service-animate">
                                <div class="service-block">
                                    <img src="public/img/serv-icon2.png" alt="">
                                    <span class="title">Mobile-friendly solutions</span>
                                    <ul>
                                        <li>&#8195;Android, iOS, WindowsPhone</li>
                                        <li>&#8195;Powerful mobile applications</li>
                                        <li>&#8195;Qualitative responsive development</li>
                                        <li>&#8195;Mobile games</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12 service-animate">
                                <div class="service-block">
                                    <img src="public/img/serv-icon3.png" alt="">
                                    <span class="title">Completely Customizable</span>
                                    <ul style="padding-right: 50px;">
                                        <li>&#8195;Intuitive CMS</li>
                                        <li>&#8195;Large number of applications</li>
                                        <li>&#8195;Manuals and instructions</li>
                                        <li>&#8195;Huge amount of themes</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12 service-animate">
                                <div class="service-block">
                                    <img src="public/img/serv-icon4.png" alt="">
                                    <span class="title">360-degree view on project</span>
                                    <ul>
                                        <li>&#8195;Wireframe</li>
                                        <li>&#8195;Prototype</li>
                                        <li>&#8195;Analysis of the structure</li>
                                        <li>&#8195;UI/UX experience</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12 service-animate">
                                <div class="service-block">
                                    <img src="public/img/serv-icon5.png" alt="">
                                    <span class="title">Clean &amp; Modern Code</span>
                                    <ul style="padding-right: 50px;">
                                        <li>&#8195;Professional php coding</li>
                                        <li>&#8195;Clear comments</li>
                                        <li>&#8195;Modern methods of coding</li>
                                        <li>&#8195;Ajax, jquery, java scripts</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12 service-animate">
                                <div class="service-block">
                                    <img src="public/img/serv-icon6.png" alt="">
                                    <span class="title">Free Updates &amp; Support</span>
                                    <ul>
                                        <li>&#8195;27/7 support</li>
                                        <li>&#8195;Free professional consultations</li>
                                        <li>&#8195;Actual free updates for your project</li>
                                        <li>&#8195;The quick answer to your questions</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!--OUR WORKS-->

        <div class="scrollto dark-work">

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class=" content works content-title scale-text">
                            <h2 class="color_303030">OUR WORKS</h2>
                            </br><img src="public/img/separate-black.png" class="divid" alt=""></br>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12 scale-text">
                        <div class="drop">
                            <input type="text" value="all works"/>
                            <a href="#" class="drop-list"></a>
                            <span>
                        <a href="#" class="filter" data-filter="all" >all works</a>
                        <a href="#" class="filter" data-filter=".travel">travel</a>
                        <a href="#" class="filter" data-filter=".entertainment">entertainment</a>
                        <a href="#" class="filter" data-filter=".e-commerce">e-commerce</a>
                        <a href="#" class="filter" data-filter=".business">business</a>
                        <a href="#" class="filter" data-filter=".company">company</a>
                        <a href="#" class="filter" data-filter=".industry">industry</a>
                    </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="full-width scale-text">
                <div class="container-mix">
                    <div class="img-work mix e-commerce">
                        <img src="public/img/work.jpg" alt="" class="onload" onload="this.setAttribute('class', 'onload loaded');">
                        <span></span>
                        <div class="works-title color_fff">
                            <i>e-commerce</i></br>
                            <b>Online shopping</b>
                        </div>
                    </div>
                    <div class="img-work mix travel">
                        <img src="public/img/work2.jpg" alt="" class="onload" onload="this.setAttribute('class', 'onload loaded');">
                        <span></span>
                        <div class="works-title color_fff">
                            <i>travel</i></br>
                            <b>City guide</b>
                        </div>
                    </div>
                    <div class="img-work mix e-commerce">
                        <img src="public/img/work.jpg" alt="" class="onload" onload="this.setAttribute('class', 'onload loaded');">
                        <span></span>
                        <div class="works-title color_fff">
                            <i>e-commerce</i></br>
                            <b>Online book base</b>
                        </div>
                    </div>
                    <div class="img-work mix business">
                        <img src="public/img/work2.jpg" alt="" class="onload" onload="this.setAttribute('class', 'onload loaded');">
                        <span></span>
                        <div class="works-title color_fff">
                            <i>business</i></br>
                            <b>Personal tailoring</b>
                        </div>
                    </div>
                    <div class="img-work mix business">
                        <img src="public/img/work.jpg" alt="" class="onload" onload="this.setAttribute('class', 'onload loaded');">
                        <span></span>
                        <div class="works-title color_fff">
                            <i>business</i></br>
                            <b>Men`s dress</b>
                        </div>
                    </div>
                    <div class="img-work mix business company">
                        <img src="public/img/work2.jpg" alt="" class="onload" onload="this.setAttribute('class', 'onload loaded');">
                        <span></span>
                        <div class="works-title color_fff">
                            <i>business</i></br>
                            <b>Coffee and tea</b>
                        </div>
                    </div>
                    <div class="img-work mix entertainment company">
                        <img src="public/img/work.jpg" alt="" class="onload" onload="this.setAttribute('class', 'onload loaded');">
                        <span></span>
                        <div class="works-title color_fff">
                            <i>entertainment</i></br>
                            <b>Music school</b>
                        </div>
                    </div>
                    <div class="img-work mix industry">
                        <img src="public/img/work2.jpg" alt="" class="onload" onload="this.setAttribute('class', 'onload loaded');">
                        <span></span>
                        <div class="works-title color_fff">
                            <i>industry</i></br>
                            <b>recovery equipment</b>
                        </div>
                    </div>
                    <div class="img-work mix business">
                        <img src="public/img/work.jpg" alt="" class="onload" onload="this.setAttribute('class', 'onload loaded');">
                        <span></span>
                        <div class="works-title color_fff">
                            <i>business</i></br>
                            <b>Financial company</b>
                        </div>
                    </div>
                    <div class="img-work mix industry company">
                        <img src="public/img/work2.jpg" alt="" class="onload" onload="this.setAttribute('class', 'onload loaded');">
                        <span></span>
                        <div class="works-title color_fff">
                            <i>industry</i></br>
                            <b>oil recycling</b>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--OUR NEWS-->

        <div class="full-width">
            <div class="scrollto blog-wraper">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="content content-title scale-text blog">
                                <h2 class="color_303030">latest news</h2>
                                <br><img src="public/img/separate-black.png" class="divid" alt="separate">
                            </div>
                        </div>
                    </div>
                    <div class="row scale-text">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="block-news blog-animate">
                                <div class="layer-wraper">
                                    <div class="layer-blog"></div>
                                    <a href="blog/index.html">
                                        <img src="public/img/news1.jpg" alt="news1" class="blog-img onload" onload="this.setAttribute('class', 'blog-img onload loaded');">
                                    </a>
                                </div>
                                <div class="title">
                                    <a href="blog/index.html">Lorem ipsum dolor sit amet</a>
                                </div>
                                <div class="date-link dark-link"><img src="public/img/date_icon.jpg" alt="clock">Mickey cutter<span class="color_green">  /  </span>october 30, 2014<span class="color_green">  /  </span><a href="">travel</a>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore, accusamus, ipsam a nam dolor earum distinctio libero vel optio ipsum iusto et ad illum nulla quae at reiciendis amet laborum!</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="block-news blog-animate">
                                <div class="layer-wraper">
                                    <div class="layer-blog"></div>
                                    <a href="blog/index.html">
                                        <img src="public/img/news2.jpg" alt="news1" class="blog-img onload" onload="this.setAttribute('class', 'blog-img onload loaded');">
                                    </a>
                                </div>
                                <div class="title">
                                    <a href="blog/index.html">Libero vel optio ipsum iusto</a>
                                </div>
                                <div class="date-link dark-link"><img src="public/img/date_icon.jpg" alt="clock">Mickey cutter<span class="color_green">  /  </span>october 30, 2014<span class="color_green">  /  </span><a href="">travel</a>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore, accusamus, ipsam a nam dolor earum distinctio libero vel optio ipsum iusto et ad illum nulla quae at reiciendis amet laborum!</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="block-news blog-animate">
                                <div class="layer-wraper">
                                    <div class="layer-blog"></div>
                                    <a href="blog/index.html">
                                        <img src="public/img/news3.jpg" alt="news1" class="blog-img onload" onload="this.setAttribute('class', 'blog-img onload loaded');">
                                    </a>
                                </div>
                                <div class="title">
                                    <a href="blog/index.html">Libero vel optio ipsum iusto</a>
                                </div>
                                <div class="date-link dark-link"><img src="public/img/date_icon.jpg" alt="clock">Mickey cutter<span class="color_green">  /  </span>october 30, 2014<span class="color_green">  /  </span><a href="">travel</a>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore, accusamus, ipsam a nam dolor earum distinctio libero vel optio ipsum iusto et ad illum nulla quae at reiciendis amet laborum!</p>
                            </div>
                        </div>
                    </div>
                    <div class="row scale-text">
                        <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
                            <div class="contact-button">
                                <a href="blog/index.html"><input type="submit" value="read all news"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--OUR TEAM-->

        <div class="scrollto">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="content content-title scale-text">
                            <h2 class="color_303030">OUR TEAM</h2>
                            </br><img src="public/img/separate-black.png" class="divid" alt="">
                            <p>tincidunt turpis, eu fringilla erat. Vestibulum lacinia, nisi sed bibendum pretium, eros sapien laoreet metus, ac facilisis felis enim a nibh. Sed sed ligula tellus. Aliquam feugiat, nunc ut tincidunt laoreet, urna quam pellentesque diam, id posuere velit lectus vel dui. Maecenas posuere eget metus ac venenatis.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row scale-text">
                    <div class="col-md-12">
                        <div class="team-container team-slider">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide team-animate">
                                    <div class="team-block">
                                        <div class="layer"></div>
                                        <div class="dark-wrapper-team-img">
                                            <img src="public/img/team.jpg" alt="" class="team-img onload" onload="this.setAttribute('class', 'team-img onload loaded');">
                                        </div>
                                        <button class="team-button color_fff">
                                            <b>John Dou</b></br>
                                            <i>company founder</i>
                                        </button>
                                        <div class="team-icon">
                                            <div class="roll-batton">
                                                <img src="public/img/team-fb.png" alt="">
                                                <img src="public/img/team-fb-hov.png" class="roll" alt="">
                                            </div>
                                            <div class="roll-batton">
                                                <img src="public/img/team-tw.png" alt="">
                                                <img src="public/img/team-tw-hov.png" class="roll" alt="">
                                            </div>
                                            <div class="roll-batton">
                                                <img src="public/img/team-in.png" alt="">
                                                <img src="public/img/team-in-hov.png" class="roll" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide team-animate">
                                    <div class="team-block">
                                        <div class="layer"></div>
                                        <div class="dark-wrapper-team-img">
                                            <img src="public/img/team.jpg" alt="" class="team-img onload" onload="this.setAttribute('class', 'team-img onload loaded');">
                                        </div>
                                        <button class="team-button color_fff">
                                            <b>Kerry Smith</b></br>
                                            <i>co-founder</i>
                                        </button>
                                        <div class="team-icon">
                                            <div class="roll-batton">
                                                <img src="public/img/team-fb.png" alt="">
                                                <img src="public/img/team-fb-hov.png" class="roll" alt="">
                                            </div>
                                            <div class="roll-batton">
                                                <img src="public/img/team-tw.png" alt="">
                                                <img src="public/img/team-tw-hov.png" class="roll" alt="">
                                            </div>
                                            <div class="roll-batton">
                                                <img src="public/img/team-in.png" alt="">
                                                <img src="public/img/team-in-hov.png" class="roll" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide team-animate">
                                    <div class="team-block">
                                        <div class="layer"></div>
                                        <div class="dark-wrapper-team-img">
                                            <img src="public/img/team.jpg" alt="" class="team-img onload" onload="this.setAttribute('class', 'team-img onload loaded');">
                                        </div>
                                        <button class="team-button color_fff">
                                            <b>Peter Chester</b></br>
                                            <i>co-founder</i>
                                        </button>
                                        <div class="team-icon">
                                            <div class="roll-batton">
                                                <img src="public/img/team-fb.png" alt="">
                                                <img src="public/img/team-fb-hov.png" class="roll" alt="">
                                            </div>
                                            <div class="roll-batton">
                                                <img src="public/img/team-tw.png" alt="">
                                                <img src="public/img/team-tw-hov.png" class="roll" alt="">
                                            </div>
                                            <div class="roll-batton">
                                                <img src="public/img/team-in.png" alt="">
                                                <img src="public/img/team-in-hov.png" class="roll" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="team-block">
                                        <div class="layer"></div>
                                        <div class="dark-wrapper-team-img">
                                            <img src="public/img/team.jpg" alt="" class="team-img onload" onload="this.setAttribute('class', 'team-img onload loaded');">
                                        </div>
                                        <button class="team-button color_fff">
                                            <b>Monica Yagoo</b></br>
                                            <i>creative director</i>
                                        </button>
                                        <div class="team-icon">
                                            <div class="roll-batton">
                                                <img src="public/img/team-fb.png" alt="">
                                                <img src="public/img/team-fb-hov.png" class="roll" alt="">
                                            </div>
                                            <div class="roll-batton">
                                                <img src="public/img/team-tw.png" alt="">
                                                <img src="public/img/team-tw-hov.png" class="roll" alt="">
                                            </div>
                                            <div class="roll-batton">
                                                <img src="public/img/team-in.png" alt="">
                                                <img src="public/img/team-in-hov.png" class="roll" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="team-block">
                                        <div class="layer"></div>
                                        <div class="dark-wrapper-team-img">
                                            <img src="public/img/team.jpg" alt="" class="team-img onload" onload="this.setAttribute('class', 'team-img onload loaded');">
                                        </div>
                                        <button class="team-button color_fff">
                                            <b>Samson Exter</b></br>
                                            <i>pr</i>
                                        </button>
                                        <div class="team-icon">
                                            <div class="roll-batton">
                                                <img src="public/img/team-fb.png" alt="">
                                                <img src="public/img/team-fb-hov.png" class="roll" alt="">
                                            </div>
                                            <div class="roll-batton">
                                                <img src="public/img/team-tw.png" alt="">
                                                <img src="public/img/team-tw-hov.png" class="roll" alt="">
                                            </div>
                                            <div class="roll-batton">
                                                <img src="public/img/team-in.png" alt="">
                                                <img src="public/img/team-in-hov.png" class="roll" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="team-block">
                                        <div class="layer"></div>
                                        <div class="dark-wrapper-team-img">
                                            <img src="public/img/team.jpg" alt="" class="team-img onload" onload="this.setAttribute('class', 'team-img onload loaded');">
                                        </div>
                                        <button class="team-button color_fff">
                                            <b>Ray Mcdonald</b></br>
                                            <i>head programmer</i>
                                        </button>
                                        <div class="team-icon">
                                            <div class="roll-batton">
                                                <img src="public/img/team-fb.png" alt="">
                                                <img src="public/img/team-fb-hov.png" class="roll" alt="">
                                            </div>
                                            <div class="roll-batton">
                                                <img src="public/img/team-tw.png" alt="">
                                                <img src="public/img/team-tw-hov.png" class="roll" alt="">
                                            </div>
                                            <div class="roll-batton">
                                                <img src="public/img/team-in.png" alt="">
                                                <img src="public/img/team-in-hov.png" class="roll" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="team-block">
                                        <div class="layer"></div>
                                        <div class="dark-wrapper-team-img">
                                            <img src="public/img/team.jpg" alt="" class="team-img onload" onload="this.setAttribute('class', 'team-img onload loaded');">
                                        </div>
                                        <button class="team-button color_fff">
                                            <b>Jess Winter</b></br>
                                            <i>head hr</i>
                                        </button>
                                        <div class="team-icon">
                                            <div class="roll-batton">
                                                <img src="public/img/team-fb.png" alt="">
                                                <img src="public/img/team-fb-hov.png" class="roll" alt="">
                                            </div>
                                            <div class="roll-batton">
                                                <img src="public/img/team-tw.png" alt="">
                                                <img src="public/img/team-tw-hov.png" class="roll" alt="">
                                            </div>
                                            <div class="roll-batton">
                                                <img src="public/img/team-in.png" alt="">
                                                <img src="public/img/team-in-hov.png" class="roll" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pagination swich_2">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--CAREER-->

        <div class="scrollto">
            <div class="full-width scale-text">
                <div class="career-container career-slider">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="bg-fix img-develop">
                                <img src="public/img/career.jpg" data-width-img="1920" data-height-img="1080"/>
                            </div>
                            <div class="slider-top-thumbs">
                                <div class="career-button color_fff career-animate">
                                    <div class="career-title">
                                        <div class="sity-carrer"><i>Washington, USA</i></div>
                                        <div class="develop"><b>senior web-developer</b></div>
                                        <div class="experience"><h4>Experience in PHP and HTML5/CSS3 is required</h4></div>
                                        <span class="career-top"></span>
                                        <span class="career-left"></span>
                                        <div class="text-insert"  data-text="It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum condimentum non enim in placerat. Cras molestie eros faucibus, ultricies justo nec, blandit magna. Sed egestas, nisi sodales mattis condimentum, dolor arcu sagittis metus, at commodo mauris tellus ut tellus. Nunc quis pulvinar mauris. Cras facilisis lorem sit amet tincidunt pretium. Integer et dignissim purus. Integer egestas euismod augue. Donec consequat metus mi, quis cursus diam feugiat a. Fusce diam enim, efficitur mattis tortor id, molestie vehicula tellus. Cras et interdum mauris, a pharetra ex. Integer aliquam purus lectus, accumsan suscipit odio hendrerit ut. Integer hendrerit finibus neque, eu tempus ante consectetur a. Curabitur blandit sapien mi, vitae scelerisque erat tincidunt ut.
Sed tristique vehicula consectetur. Integer dignissim facilisis eros. Duis venenatis ornare lacus in vestibulum. Aenean nisi sem, bibendum vel felis ac, facilisis fermentum lacus. Nulla ac mi non justo convallis rutrum. Nunc vitae sagittis arcu. Vestibulum a ipsum ipsum. Curabitur id nunc libero.Ut risus magna, suscipit non convallis ut, tincidunt nec purus. Donec scelerisque hendrerit massa facilisis rutrum. Cras maximus dolor nec neque blandit, sit amet vehicula lorem volutpat. Morbi venenatis, elit vel faucibus eleifend, sapien nisi maximus libero, sed consequat nisi elit eu lorem. Integer bibendum ex ut est venenatis facilisis. Morbi nec facilisis turpis. Aenean leo risus, aliquet ac faucibus vel, scelerisque nec elit. Mauris sed tincidunt massa. Pellentesque eget sodales lorem. Donec ut egestas metus. Sed eget felis eu metus sollicitudin interdum. Mauris et enim ac risus suscipit sollicitudin vitae et mauris. Aliquam ac sem hendrerit, sagittis mi a, cursus eros. Sed sed felis sit amet sem porttitor tristique."></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="bg-fix img-develop">
                                <img src="public/img/career.jpg" data-width-img="1920" data-height-img="1080"/>
                            </div>
                            <div class="slider-top-thumbs">
                                <div class="career-button color_fff career-animate">
                                    <div class="sity-carrer"><i>Washington, USA</i></div>
                                    <div class="develop"><b>Junior JAVA-developer</b></div>
                                    <div class="experience"><h4>Experience 1-2 years, Java, JSP/Servlets, EJB, CSS, XML, HTML5</h4></div>
                                    <span class="career-top"></span>
                                    <span class="career-left"></span>
                                    <div class="text-insert"  data-text="There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.Aliquam quis viverra tellus. Suspendisse ante ex, malesuada ut euismod ut, varius consectetur justo. Nunc a varius felis, in iaculis elit. Sed viverra sapien non scelerisque volutpat. Sed egestas eros ut tincidunt commodo. Pellentesque non nibh tempus, consectetur est vel, placerat massa. Etiam vel metus non arcu finibus consequat id et neque. Donec mi nisl, efficitur ac quam ac, sollicitudin fermentum nulla. Aenean nec urna eget metus vehicula lobortis. Nunc mollis diam vel eleifend lacinia. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec enim tortor, malesuada nec convallis non, pulvinar non est. Mauris a elit non risus efficitur iaculis quis sed nisl.

Fusce ut eleifend enim. Vivamus at bibendum tortor, nec lacinia turpis. Proin feugiat tortor et metus aliquet ultricies. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum dignissim orci non molestie malesuada. Integer felis turpis, ornare egestas interdum ac, dapibus lobortis sapien. Nam eleifend aliquet justo non ullamcorper. Ut varius quam tellus. Aenean maximus auctor suscipit."></div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="bg-fix img-develop">
                                <img src="public/img/career.jpg" data-width-img="1920" data-height-img="1080"/>
                            </div>
                            <div class="slider-top-thumbs">
                                <div class="career-button color_fff career-animate">
                                    <div class="sity-carrer"><i>Toronto, Canada</i></div>
                                    <div class="develop"><b>Graphic Designer</b></div>
                                    <div class="experience"><h4>Experience 1-2 years, Photoshop CS6, Adobe Illustrator, CoralDraw, draw by hand</h4></div>
                                    <span class="career-top"></span>
                                    <span class="career-left"></span>
                                    <div class="text-insert" data-text="Phasellus ornare condimentum cursus. Nulla sit amet mi quis neque posuere vehicula. Proin odio nunc, cursus id facilisis sit amet, cursus eget lectus. Etiam venenatis lacus non tortor aliquet tempor. Donec pharetra leo a mi malesuada volutpat. Aenean eleifend ut orci vel pharetra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed posuere libero tortor, et porttitor elit venenatis id. Cras eu nisi diam. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris finibus lectus purus, dictum dictum nibh hendrerit et. Aenean quis congue elit, vel commodo augue. Quisque eleifend interdum enim non rutrum. Vivamus vitae arcu lacus. Phasellus nisl leo, feugiat id efficitur vitae, condimentum sed urna.Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent id molestie nibh. Donec ornare orci eget tempor vehicula. Cras felis velit, aliquet eu consequat quis, ullamcorper ut velit. Curabitur dolor magna, varius aliquam nisl vitae, interdum euismod sapien. Praesent sapien mauris, viverra in sagittis a, pharetra ac leo. Fusce laoreet faucibus est, a ultrices risus cursus id. Phasellus ac orci nisl. Vivamus maximus suscipit urna, vel viverra sem fermentum vitae. Fusce non nulla quis turpis tempus pharetra non ut lorem. Fusce pulvinar accumsan arcu, eu congue urna varius id.

Proin vehicula nibh sit amet eros pharetra pharetra. Morbi sed magna ac nibh posuere consectetur ac egestas libero. Pellentesque molestie ornare leo non mattis. Morbi interdum quis mi non rhoncus. Nam efficitur at nisi id faucibus. Cras lobortis lacus cursus, pulvinar urna eu, dictum ligula. Aenean quis massa erat. Aliquam placerat urna vitae fringilla pulvinar. Nam fringilla velit vitae varius molestie. Interdum et malesuada fames ac ante ipsum primis in faucibus. Suspendisse varius aliquet urna quis vestibulum. Nullam sit amet massa ex. Etiam rutrum volutpat magna."></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slider-prev">&#212;</div>
                    <div class="slider-next">&#215;</div>
                    <div class="pagination swich_3">
                    </div>
                </div>
            </div>
        </div>

        <!--CLIENTS-->

        <div class="scrollto">
            <div class="full-width bg_5acdc4 logotype">
                <div class="logo-company full-width">
                    <div class="client-continer client-slider">
                        <div class="swiper-wrapper client">
                            <div class="swiper-slide">
                                <img src="public/img/logo.jpg" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="public/img/logo.jpg" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="public/img/logo.jpg" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="public/img/logo.jpg" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="public/img/logo.jpg" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="public/img/logo.jpg" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="public/img/logo.jpg" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="public/img/logo.jpg" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="public/img/logo.jpg" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="public/img/logo.jpg" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="public/img/logo.jpg" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="public/img/logo.jpg" alt="">
                            </div>
                        </div>


                    </div>
                    <div class="slider-prev">&#212;</div>
                    <div class="slider-next">&#215;</div>
                </div>
            </div>
            <div class="full-width">
                <div class="bg">
                    <img class="center_image" src="public/img/client.jpg" data-width="1920" data-height="1080"/>
                </div>
                <div class="container">
                    <div class="row scale-text">
                        <div class="col-md-12">
                            <div class="people-container people-slider">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide people">
                                        <div class="people-wrap color_303030">
                                            <img src="public/img/people1.jpg" alt="" class="man">
                                            <p>Praesent leo risus, bibendum ut ante a, auctor commodo diam. Maecenas sed mi convallis, auctor est sed, fermentum massa. Nulla porta magna augue, id volutpat tellus ultrices quis. Phasellus lacinia nisi vitae mattis venenatis. Phasellus eget convallis sem, at viverra tellus. Integer non sem sit amet lectus lobortis molestie nec eget felis. Maecenas porttitor condimentum hendrerit.
                                            </p>
                                            <b class="inc">Steve McApple, Building Compnay Inc.</b>
                                        </div>
                                    </div>

                                    <div class="swiper-slide people">
                                        <div class="people-wrap color_303030">
                                            <img src="public/img/people1.jpg" alt="" class="man">
                                            <p>Praesent leo risus, bibendum ut ante a, auctor commodo diam. Maecenas sed mi convallis, auctor est sed, fermentum massa. Nulla porta magna augue, id volutpat tellus ultrices quis. Phasellus lacinia nisi vitae mattis venenatis. Phasellus eget convallis sem, at viverra tellus. Integer non sem sit amet lectus lobortis molestie nec eget felis. Maecenas porttitor condimentum hendrerit.Phasellus lacinia nisi vitae mattis venenatis. Phasellus eget convallis sem, at viverra tellus.
                                            </p>
                                            <b class="inc">Zlata Rock, Rock Systems</b>
                                        </div>
                                    </div>

                                    <div class="swiper-slide people">
                                        <div class="people-wrap color_303030">
                                            <img src="public/img/people1.jpg" alt="" class="man">
                                            <p>Praesent leo risus, bibendum ut ante a, auctor commodo diam. Maecenas sed mi convallis, auctor est sed, fermentum massa. Nulla porta magna augue, id volutpat tellus ultrices quis. Phasellus lacinia nisi vitae mattis venenatis. Phasellus eget convallis sem, at viverra tellus. Integer non sem sit amet lectus lobortis molestie nec eget felis. Maecenas porttitor condimentum hendrerit.
                                            </p>
                                            <b class="inc">Dorian Gray, IT Security</b>
                                        </div>
                                    </div>
                                </div>
                                <div class="pagination swich_4">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--CONTACT-->

        <div class="scrollto">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="content content-title contact scale-text">
                            <h2 class="color_303030 timeIn">Connect with us</h2>
                            </br><img src="public/img/separate-black.png" class="divid timeIn" alt="">
                        </div>
                    </div>
                </div>

                <div class="row scale-text">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 bg_5acdc4 contact-animate">
                        <div class="contact-block color_fff">
                            <b>Markham</b>
                            </br><adress>
                                </br>2800 14th Ave. Suite 405
                                </br>Markham, Ontario
                                </br>Canada, L3R 0E4
                                </br>TEL: 416.703.9704
                            </adress>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 bg_3ab2a9 contact-animate">
                        <div class="contact-block color_fff">
                            <b>Toronto</b>
                            </br><adress>
                                </br>20 Bay Street Suite 1205
                                </br>Toronto, Ontario
                                </br>Canada, M5J 2N8
                                </br>TEL: 416.703.9704
                            </adress>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 clear-r bg_5acdc4 contact-animate">
                        <div class="contact-block color_fff">
                            <b>New York</b>
                            </br><adress>
                                </br>244 5th Avenue. Suite 1735
                                </br>New York, N.Y.
                                </br>USA, 10001
                                </br>TEL: 347.759.6203
                            </adress>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 bg_3ab2a9 contact-animate">
                        <div class="contact-block color_fff">
                            <b>Naples</b>
                            </br><adress>
                                </br>850 Central Ave. Unit 102
                                </br>Naples, FL
                                </br>USA, 34102
                                </br>   TEL: 347.759.6203
                            </adress>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="icon scale-text">
                            <div class="soc-icon blue">
                                <a href="">
                                    <img src="public/img/contact-icon1.png" alt="">
                                    <img src="public/img/contact-icon1_1.png" class="icon-hov" alt="">
                                </a>
                            </div>
                            <div class="soc-icon blue2">
                                <a href="">
                                    <img src="public/img/contact-icon2.png" alt="">
                                    <img src="public/img/contact-icon2_1.png" class="icon-hov" alt="">
                                </a>
                            </div>
                            <div class="soc-icon pink">
                                <a href="">
                                    <img src="public/img/contact-icon3.png" alt="">
                                    <img src="public/img/contact-icon3_1.png" class="icon-hov" alt="">
                                </a>
                            </div>
                            <div class="soc-icon brown">
                                <a href="">
                                    <img src="public/img/contact-icon4.png" alt="">
                                    <img src="public/img/contact-icon4_1.png" class="icon-hov" alt="">
                                </a>
                            </div>
                            <div class="soc-icon red">
                                <a href="">
                                    <img src="public/img/contact-icon5.png" alt="">
                                    <img src="public/img/contact-icon5_1.png" class="icon-hov" alt="">
                                </a>
                            </div>
                            <div class="soc-icon blue3">
                                <a href="">
                                    <img src="public/img/contact-icon6.png" alt="">
                                    <img src="public/img/contact-icon6_1.png" class="icon-hov" alt="">
                                </a>
                            </div>
                            <div class="soc-icon blue4">
                                <a href="">
                                    <img src="public/img/contact-icon7.png" alt="">
                                    <img src="public/img/contact-icon7_1.png" class="icon-hov" alt="">
                                </a>
                            </div>
                            <div class="soc-icon red2">
                                <a href="">
                                    <img src="public/img/contact-icon8.png" alt="">
                                    <img src="public/img/contact-icon8_1.png" class="icon-hov" alt="">
                                </a>
                            </div>
                            <div class="soc-icon blue5">
                                <a href="">
                                    <img src="public/img/contact-icon9.png" alt="">
                                    <img src="public/img/contact-icon9_1.png" class="icon-hov" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row full-width-input scale-text">
                            <div class="col-md-6 col-sm-12">
                                <div class="contact-field">
                                    <input type="text" value="Name*&emsp;&emsp;" class="timeIn">
                                    <div class="drop-con">
                                        <input type="text" value="Departure*" class="timeIn">
                                        <a href="" class="con-list"></a>
                                        <span>
                                <a href="#">travel</a>
                                <a href="#">personal</a>
                                <a href="#">e-commerce</a>
                                <a href="#">promo</a>
                                <a href="#">company</a>
                            </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="contact-field">
                                    <div class="error-input"><input type="email" value="Email*&emsp;&emsp;" class="timeIn"></div>
                                    <input type="text" value="Subject&emsp;&emsp;" class="timeIn">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row scale-text">
                    <div class="col-md-12">
                        <div class="contact-massege">
                            <textarea name="" placeholder="Message..." class="timeIn"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row scale-text">
                    <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
                        <div class="contact-button">
                            <input type="submit" value="submit" class="th timeIn">
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div class="full-width">
                    <div class="map-canvas" id="map-canvas-contact" data-lat="38.8996159" data-lng="-77.0451223" data-lat2="38.8911862" data-lng2="-77.1439894" data-lat3="38.879738" data-lng3="-76.951597" data-zoom="12" data-string="<b>Naples</b></br><adress></br>850 Central Ave. Unit 102</br>Naples, FL</br>USA, 34102</br>   TEL: 347.759.6203">
                    </div>
                </div>
                <div class="full-width">
                    <div class="copyright">
                        <i>&copy; 2014 by Invisio. All rights reserved.</i>
                    </div>
                </div>
            </div>

        </div>

        {include file="./messages/popup/popup-works.tpl"}
        {include file="./messages/popup/popup-teams.tpl"}
        {include file="./messages/popup/popup-career.tpl"}
        {include file="./messages/popup/popup-thanks.tpl"}

    </div>
</div>
</body>
{include file="./static/footer.tpl"}